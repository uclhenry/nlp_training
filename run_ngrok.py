import os
import subprocess
from time import sleep

import numpy as np
import pandas as pd

abs_file = os.path.abspath(__file__)
static_path = os.path.dirname(abs_file)
print(static_path)
from sequence_labeling_util import *
# from autonlp.seq_labelling.corpus import *
# from autonlp.seq_labelling.processor import read_seq_label_bio_data
from client_nlp import *
from ngrok_config import *


def get_ngrok_cmd(service):
    subdomain, ip, port = subdomains[service], ips[service], ports[service]
    if service in subdomain:
        return "./ngrok -config=ngrok.cfg -subdomain={0} {1}:{2}".format(subdomain, ip, port)


def cd_ngrok(ip):
    os.chdir(ngrok_dirs[ip])


def cd_nlp_service(service):
    os.chdir(servers[service])


def run_ngrok(subdomain, ip, port):
    cmd = "./ngrok -config=ngrok.cfg -subdomain={0} {1}:{2}".format(subdomain, ip, port)
    os.system(cmd)


def run_cmd_get_rsp(cmd):
    res = os.popen(cmd)
    output_str = res.read()  # 获得输出字符串
    return (output_str)


def show_map(ip, port, subdomain, route='autonlp_seqlabelling'):
    print("inner api at http://{}:{}/{}".format(ip, port, route))
    # print("map to http://{}.ngrok.laipy.org:5442/{}".format(subdomain,route))
    # print("map to http://{}.ngrok.laipy.org:5442/{}".format(subdomain,route))


def runsub(args, subdomain_, route):
    p = subprocess.run(args=args, timeout=3.0, shell=True)
    print(p)
    if p.returncode == 0:
        ngrok_url = "http://{}.ngrok.laipy.org:5442/{}".format(subdomain_, route)
        print('success')
        return ngrok_url
    else:
        print("ngrok service fail!")
        return None


def start_local_sl(host, port, subdomain_):
    service_type = "seq"
    s = Screen(service_type + "_" + str(port), True)
    s.send_commands("cd {}".format(ngrok_dirs[host]))
    s.send_commands("./ngrok -config=ngrok.cfg -subdomain={0} {1}:{2}".format(subdomain_, host, port))
    show_map(host, port, subdomain_)
    return s


def start_diy_local_sl(host, port, subdomain_):
    service_type = "seq"
    s = MyScreen(service_type + "_" + str(port), True)
    s.send_commands("cd {}".format(ngrok_dirs[host]))
    s.send_commands("./ngrok -config=ngrok.cfg -subdomain={0} {1}:{2}".format(subdomain_, host, port))
    show_map(host, port, subdomain_)
    return s


def get_pids_from_search_ngrok(rsp):
    pids = []
    for line in rsp.split("\n"):
        print(line)
        pid = line.split()[1] if len(line.split()) > 2 else None
        print(pid)
        if pid:
            pids.append(pid)
    return pids


def get_pids_from_screen_rsp(rsp):
    pids = []
    for line in rsp.split("\n"):
        if '\t' in line:
            line = line.split('\t')
            # print(line[0],line[1])
            if len(line[1].split('.')) >= 2:
                print(line[1])
                pid = line[1].split('.')[0]
                screenname = '.'.join(line[1].split('.')[1:])
                print(screenname)
                print(pid)
                pids.append(pid)
            else:
                continue
    return pids


def get_pids_from_screen(rsp, job):
    pids = []
    for line in rsp.split("\n"):
        if '\t' in line:
            line = line.split('\t')
            # print(line[0],line[1])
            if len(line[1].split('.')) >= 2:
                print(line[1])
                pid = line[1].split('.')[0]
                screenname = '.'.join(line[1].split('.')[1:])
                print(screenname)
                print(pid)
                if screenname == job:
                    pids.append(pid)
            else:
                continue
    return pids


def kill_screen_service(service):
    job = "{}_{}".format(service, ports[service])
    rsp = run_cmd_get_rsp('screen -ls')
    if 'No Sockets' in rsp:
        print(rsp)
    else:
        pids = get_pids_from_screen(rsp, job)
        if len(pids) > 0:
            os.system("kill -9 {}".format(" ".join(pids)))
            os.system("screen -wipe")


def clear_all_screen():
    rsp = run_cmd_get_rsp('screen -ls')
    if 'No Sockets' in rsp:
        print(rsp)
    else:
        pids = get_pids_from_screen_rsp(rsp)
        if len(pids) > 0:
            os.system("kill -9 {}".format(" ".join(pids)))
            os.system("screen -wipe")


def get_host_service_types():
    return [u for u in ips.keys() if ips[u] == get_host_ip()]


def clear_all_ngrok():
    # ps - ef | grep
    # ngrok
    rsp = run_cmd_get_rsp('ps -ef|grep ngrok')
    if 'No Sockets' in rsp:
        print(rsp)
    else:
        pids = get_pids_from_search_ngrok(rsp)
        os.system("kill -9 {}".format(" ".join(pids)))
        # os.system("screen -wipe")


def clear_process(name):
    rsp = run_cmd_get_rsp('ps -ef|grep {}'.format(name))
    if 'No Sockets' in rsp:
        print(rsp)
    else:
        pids = get_pids_from_search_ngrok(rsp)
        os.system("kill -9 {}".format(" ".join(pids)))


def start_service(service_type):
    if service_type not in get_host_service_types():
        return None
    port = ports[service_type]
    subdomain_ = subdomains[service_type]
    host = ips[service_type]
    s = Screen(service_type + "_" + str(port), True)
    s.send_commands("cd {}".format(ngrok_dirs[host]))
    s.send_commands("pwd")
    s.send_commands("./ngrok -config=ngrok.cfg -subdomain={0} {1}:{2}".format(subdomain_, host, port))
    return s


def start_diy_service(service_type):
    port = ports[service_type]
    subdomain_ = subdomains[service_type]
    host = ips[service_type]
    s = MyScreen(service_type + "_" + str(port), True)
    s.send_commands("./ngrok -config=ngrok.cfg -subdomain={0} {1}:{2}".format(subdomain_, host, port))
    return s


from screenutils import Screen


class MyScreen(Screen):
    def send_commands_return(self, *commands):
        """send commands to the active gnu-screen"""
        self._check_exists()
        for command in commands:
            self._screen_commands('stuff "' + command + '" ',
                                  'eval "stuff \\015"')

    def _screen_commands(self, *commands):
        """allow to insert generic screen specific commands
        a glossary of the existing screen command in `man screen`"""
        self._check_exists()
        for command in commands:
            print(command)
            info = run_cmd_get_rsp('screen -x ' + self.id + ' -X ' + command)
            print(info)
            sleep(3)


def service_request(service_type):
    if service_type not in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        raise EnvironmentError('current does not support {}'.format(service_type))
    ip = ips[service_type]
    port = ports[service_type]
    route = routes[service_type]
    ner_client = AutoNerClient(ip, port, route)
    autonlp_demo_request_data = autonlp_demo_request_datas[service_type]
    faq_rsp_data = ner_client.nlp_request(autonlp_demo_request_data)
    if 'error' in faq_rsp_data:
        print(' error ', faq_rsp_data['error'])
        return 'error'
    else:
        # pprint.pprint(faq_rsp_data)
        return 'success'


def request_with_config(service_type, ip, port, route, xs):
    nlp_client = AutoNerClient(ip, port, route)
    autonlp_demo_request_data = autonlp_demo_request_datas[service_type]
    if service_type != 'qp' and service_type != 'qa':
        # xs is [seq,seq,seq]
        if xs is None or xs == []:
            return {}
        if isinstance(xs[0], list):
            xs = [''.join(x) for x in xs]
    autonlp_demo_request_data['task_data'] = xs
    rsp_data = nlp_client.nlp_request(autonlp_demo_request_data)
    return rsp_data


def public_service_request(service_type):
    if service_type not in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        raise EnvironmentError('current does not support [{}]'.format(service_type))
    ip = ips[service_type]
    port = ports[service_type]
    route = routes[service_type]
    ner_client = AutoNerClient(ip, port, routes[service_type])
    url = "http://{}.ngrok.metadl.com:5442/{}"
    ner_client.autonlp_demo_url = url.format(subdomains[service_type], routes[service_type])
    print(ner_client.autonlp_demo_url)
    autonlp_demo_request_data = autonlp_demo_request_datas[service_type]
    faq_rsp_data = ner_client.nlp_request(autonlp_demo_request_data)
    if 'error' in faq_rsp_data:
        print(' error ', faq_rsp_data['error'])
        return 'error'
    else:
        pprint.pprint(faq_rsp_data)
        return 'success'


def request_all_service():
    result = {}
    for service in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        print("starting ", service)
        state = service_request(service)
        result[service] = state
    return result


def public_request_all_service():
    result = {}
    for service in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        state = public_service_request(service)
        result[service] = state
    return result


def start_server(service):
    cd_nlp_service(service)
    os.system("nohup {} &".format(nlp_cmds[service]))


def get_nlp_process(service, rsp):
    print("find the service", service)
    pids = []
    for line in rsp.split("\n"):
        # print(line)
        # print('='*30)
        pid = line.split()[1] if len(line.split()) > 2 else None
        infos = line.split()
        # for  ix,info in enumerate(infos):
        #     print(ix,info)
        if len(infos) >= 9:
            # print(infos[7])
            if nlp_pys[service] in infos[8] and servers[service] in infos[8]:
                pids.append(pid)
                print(service)
    return pids


def get_nlp_service_pid(service):
    cmd = "lsof -i :{}".format(ports[service])
    print(cmd)
    rsp = run_cmd_get_rsp(cmd)
    for line in rsp.split('\n'):
        print(line)
        if len(line.split()) <= 2: continue
        if line.split()[0] == 'python':
            return [int(line.split()[1])]

    return []


def check_service(service):
    rsp = run_cmd_get_rsp('ps -ef|grep python')
    if 'No Sockets' in rsp:
        print(rsp)
        return False
    else:
        # pids = get_nlp_process(service,rsp)
        pids = get_nlp_service_pid(service)
        print("pids", pids)
        if len(pids) > 0:
            return True


def evaluate_online_model(data_tuple, service_type, request_config):
    xs = [x for x, y in data_tuple]
    ys = [y for x, y in data_tuple]
    if service_type == 'seq':
        seqs = []
        span_line_tags = []
        n = 0
        # max_n = 10
        print('test data has rows of ', len(data_tuple))
        for seq, seq_tags in data_tuple:
            span_format_result = word_tag_pair2tencent_format(seq, seq_tags)
            seqs.append(seq)
            span_line_tags.append(span_format_result)

        # assume xs is [words,words,words...]
        ip, port, route = request_config['ip'], request_config['port'], request_config['route']
        rsp = request_with_config(service_type, ip, port, route, xs)
        predict_entities = [d['entities'] for d in rsp['predict_result']]
        tc_predict = [rsp_entity_to_tc_fromat(row) for row in predict_entities]
        f1 = compute_f1_by_token(tc_predict, span_line_tags, ner=True)
        score = f1
    # predict_result = batch_request_service(xs,service_type,request_config)
    elif service_type == 'en_cls' or service_type == 'zh_cls':
        xs = [x for x, y in data_tuple]
        ys = [y for x, y in data_tuple]
        ip, port, route = request_config['ip'], request_config['port'], request_config['route']
        rsp = request_with_config(service_type, ip, port, route, xs)
        predict_ys = [np.argmax(y) for y in rsp['predict_results']]
        predict_labels = rsp['predict_labels']
        print(predict_ys)
        print(predict_labels)
        same = [1 for ix, y in enumerate(ys) if y == predict_ys[ix]]
        acc = len(same) / len(ys)
        score = acc
        print('acc', acc)
    elif service_type == 'qp':
        ip, port, route = request_config['ip'], request_config['port'], request_config['route']
        qs = [x[0] for x, y in data_tuple]
        ds = [x[1] for x, y in data_tuple]
        ys = [y for x, y in data_tuple]
        scores = []
        for x, y in data_tuple:
            q, d = x
            task_data = {
                "q_text": q,
                "d_text_list": [
                    d
                ]
            }
            rsp = request_with_config(service_type, ip, port, route, task_data)
            '''
                "predict_results": {
                    "ranking_result": [
                        {
                            "doc_id": 0,
                            "score": 0.0569
                        }
                    ]
                }
            '''
            score = rsp['predict_results']['ranking_result'][0]['score']
            print(score)
            scores.append(score)
        scores_to_polarity = [1 if x > 0.5 else 0 for x in scores]
        print("qp scores:", ys)
        print("qp predicted scores:", scores_to_polarity)
        same = [1 for ix, y in enumerate(ys) if y == scores_to_polarity[ix]]
        acc = len(same) / len(ys)
        score = acc
        print('acc', acc)

    elif service_type == 'qa':
        '''
            "task_data": {
                "q_text": "what is florence nightingale famous for ?",
                "d_text_list": [
                    "the newly named `` corner shop '' where asprey has traded since     1847 occupies what were once nine georgian houses , one of which was the last london home of nursing pioneer florence nightingale .",
                    "in 1820 , the founder of modern nursing , florence nightingale , was born in florence , italy .",
                    "1836 _ theodor fliedner reinstitutes the order of deaconesses and opens a small hospital and training school in kaiserwerth , germany , where florence nightingale , `` the founder of modern nursing , '' receives her training .",
                    "florence nightingale , a national heroine in the united kingdom , was the pioneer of the modern nursing .",
                    "referring to the general acceptance of peplau 's interpersonal process , sills added , `` it has been argued that dr . peplau 's life and work produced the greatest changes in nursing practice since florence nightingale . ",
                    "he noted that 21 people from 12 countries , including the three chinese nurses , won the 37th nightingale awards presented by the international committee of the red cross .",
                    "the nightingale award , established at the ninth international red cross conference in washington in 1912 , are presented every two years ."
                ]
            }
        '''
        ip, port, route = request_config['ip'], request_config['port'], request_config['route']
        qs = [x[0] for x, y in data_tuple]
        ds = [x[1] for x, y in data_tuple]
        ys = []
        dys = [dy for x, dy in data_tuple]
        for dy in dys:
            ys.extend(dy)
        scores = []
        for x, y in data_tuple:
            q, d = x
            task_data = {
                "q_text": q,
                "d_text_list": d

            }
            rsp = request_with_config(service_type, ip, port, route, task_data)
            '''
             "predict_results": {
                    "ranking_result": [
                        {
                            "doc_id": 1,
                            "score": 1.0
                        },
                        {
                            "doc_id": 2,
                            "score": 0.9423
                        },
                        {
                            "doc_id": 3,
                            "score": 0.8581
                        },
                        {
                            "doc_id": 0,
                            "score": 0.5794
                        },
                        {
                            "doc_id": 4,
                            "score": 0.4132
                        },
                        {
                            "doc_id": 5,
                            "score": 0.0421
                        },
                        {
                            "doc_id": 6,
                            "score": 0.0
                        }
                    ]
                }
            '''
            score_ds = rsp['predict_results']['ranking_result']
            document_scores = [d['score'] for d in score_ds]

            print(document_scores)
            scores.extend(document_scores)
        scores_to_polarity = [1 if x > 0.5 else 0 for x in scores]
        print("qp scores:", ys)
        print("qp predicted scores:", scores_to_polarity)
        same = [1 for ix, y in enumerate(ys) if y == scores_to_polarity[ix]]
        acc = len(same) / len(ys)
        score = acc
        print('acc', acc)
    # score = compute_metric(service_type,ys,predict_result)
    # qp f1
    # qa map
    return score


def rsp_entity_to_tc_fromat(entities):
    '''
    our service format
                    {
                    "tag": "nr",
                    "tokens": "马化腾",
                    "offset": 0,
                    "chinese": "人名"
                }
    tencent format {'BeginOffset': 12, 'Type': 'ORG', 'Word': '海军航空兵12师', 'Length': 8}
    :param entities:
    :return:
    '''
    tcs = []
    for entity in entities:
        # print(entity)
        tc_entity = {}
        tc_entity['Type'] = entity['tag']
        tc_entity['BeginOffset'] = entity['offset']
        tc_entity['Word'] = entity['tokens']
        tc_entity['Length'] = len(entity['tokens'])
        tcs.append(tc_entity)
    return tcs


# def batch_request_service(xs,service_type,request_config):
#     if service_type == 'seq':
#         seqs = []
#         span_line_tags = []
#         seq_tags_lines = []
#         n = 0
#         # max_n = 10
#         print('test data has rows of ', len(test_data))
#         for seq, seq_tags in test_data:
#             span_format_result = word_tag_pair2tencent_format(seq, seq_tags)
#             seqs.append(seq)
#             seq_tags_lines.append(seq_tags)
#             span_line_tags.append(span_format_result)
#     elif service_type in ['zh_cls','en_cls']:
#         pass
#     elif service_type in ['qa','qp']:
#         pass

def compute_metric(service_type, ys, predict_result):
    pass


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--clear", default='', type=str, required=False,
                        help="ngrok/screen")
    parser.add_argument("--inf", default='', type=str, required=False,
                        help="seqlab")
    parser.add_argument("--service", default='', type=str, required=False,
                        help="start a service")
    parser.add_argument("--data_path", default='', type=str, required=False,
                        help="data_path")
    parser.add_argument("--request", default='', type=str, required=False,
                        help="all/en_cls/zh_cls,qa,qp,seq")
    parser.add_argument("--batch_req", default='', type=str, required=False,
                        help="all/en_cls/zh_cls,qa,qp,seq")
    parser.add_argument("--reset_python", action='store_true', required=False,
                        help="reset python nlp server")
    parser.add_argument("--check_python", action='store_true', required=False,
                        help="check python nlp server")
    parser.add_argument("--public_request", default='', type=str, required=False,
                        help="all/en_cls/zh_cls,qa,qp,seq")
    parser.add_argument("--fullreq", action='store_true', required=False,
                        help="full request ,inner and public")
    parser.add_argument("--reset_ngrok", action='store_true', required=False,
                        help="")
    parser.add_argument("--local_inf", action='store_true', required=False,
                        help="")
    args = parser.parse_args()
    # --clear ngrok/screen/both
    if args.check_python == True:
        services = get_host_service_types()
        print("services [{}] ".format(services))
        for service in services:
            state = check_service(service)
            if not state:
                print("[{}] is not online.".format(service))
                if args.reset_python:
                    start_server(service)
                    print("restart the Python server for {}".format(service))
            else:
                print("[{}] is running normally.".format(service))
            print('\n')

    if args.clear != '':
        if args.clear == 'ngrok':
            clear_all_ngrok()
        elif args.clear == 'screen':
            clear_all_screen()
        elif args.clear == 'both':
            clear_all_screen()
            clear_all_ngrok()
        else:
            clear_process(args.clear)

    # --inf seqlab
    elif args.inf == 'diy':
        port = 38002
        subdomain_ = "local_seqlab_inf"
        host = "192.168.21.67"
        s = start_diy_local_sl(host, port, subdomain_)
    elif args.inf == 'seqlab':
        # start seqlabelling
        while True:
            port = 38002
            subdomain_ = "local_seqlab_inf"
            host = "192.168.21.67"
            s = start_local_sl(host, port, subdomain_)
            if s:
                print(s.status)
                if s.status == 'Detached':
                    break
    elif args.batch_req in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        if args.data_path != '':
            print('read local data file and test')
            online_test_general(args.batch_req, args.data_path)
        else:
            print('default data test ')
            online_test_general(args.batch_req, '')
    elif args.inf in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        kill_screen_service(args.inf)
        start_service(args.inf)
    elif args.inf == 'all':
        clear_all_ngrok()
        clear_all_screen()
        for service in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
            start_service(service)
    elif args.service != '':
        if args.service in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
            start_server(args.service)
        if args.service == 'all':
            for s in get_host_service_types():
                start_server(s)
    elif args.request == 'all':
        res = request_all_service()
        print(res)
    elif args.request in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        print('inner service request')
        state = service_request(args.request)
        print(state)
    elif args.public_request == 'all':
        res = public_request_all_service()
        print(res)
    elif args.public_request in ['en_cls', 'zh_cls', 'qa', 'qp', 'seq']:
        print('public service request  <{}>'.format(args.public_request))
        public_service_request(args.public_request)
    elif args.local_inf == True:
        start_service('local')
    elif args.fullreq == True:
        inner_res = request_all_service()
        public_res = public_request_all_service()
        for k in inner_res:
            url = "http://{}.ngrok.metadl.com:5442/{}"
            autonlp_demo_url = url.format(subdomains[k], routes[k])
            if args.reset_ngrok == True:
                if inner_res[k] == 'success' and public_res[k] == 'error':
                    print('restart the ngrok config')
                    kill_screen_service(k)
                    start_service(k)

            print(autonlp_demo_url)
            print("local service and public service  for {}: {} ,{}".format(k, inner_res[k], public_res[k]))


import socket


def get_host_ip():
    """
    查询本机ip地址
    :return: ip
    """
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
    return ip


# def get_service_test_data(service):
#     if service == 'en_cls':
#         pass
#     elif service == 'zh_cls':
#         pass
#     elif service == 'qa':
#         pass
#     elif service == 'qp':
#         pass
#     elif service == 'seq':
#         pass

def get_service_test_data(service):
    if service == 'en_cls':
        test_data = [('this is my iphone', 7),
                     ('I like jogging and running ,do you know the competition holding on Friday?', 0)]
        return test_data
    elif service == 'zh_cls':
        pass
    elif service == 'qa':
        ds1 = [
            "the newly named `` corner shop '' where asprey has traded since     1847 occupies what were once nine georgian houses , one of which was the last london home of nursing pioneer florence nightingale .",
            "in 1820 , the founder of modern nursing , florence nightingale , was born in florence , italy .",
            "1836 _ theodor fliedner reinstitutes the order of deaconesses and opens a small hospital and training school in kaiserwerth , germany , where florence nightingale , `` the founder of modern nursing , '' receives her training .",
            "florence nightingale , a national heroine in the united kingdom , was the pioneer of the modern nursing ."]
        ds2 = [
            "referring to the general acceptance of peplau 's interpersonal process , sills added , `` it has been argued that dr . peplau 's life and work produced the greatest changes in nursing practice since florence nightingale . ",
            "he noted that 21 people from 12 countries , including the three chinese nurses , won the 37th nightingale awards presented by the international committee of the red cross .",
            "the nightingale award , established at the ninth international red cross conference in washington in 1912 , are presented every two years ."

        ]
        test_data = [(("what is florence nightingale famous for ?", ds1), [1, 1, 1]),
                     (("what is florence nightingale famous for ?", ds2), [1, 0, 0, 0])]
        return test_data
    elif service == 'qp':
        test_data = [(('借呗额度多少', '借呗能借多少钱'), 1), (('借呗额度多少', '借呗怎么打开'), 0)]
        return test_data
    elif service == 'seq':
        p = processors['boson']
        filename = os.path.join(p.data_dir, p.test_filename)
        test_data = read_seq_label_bio_data(filename, 0, 1, output_alibaba=False, language='zh')
        test_data = test_data[:200]
        return test_data


def service_data_reader(data_path, service):
    if service == 'en_cls' or service == 'zh_cls':
        csv_df = pd.read_excel(data_path, encoding='utf-8')
        ys = csv_df['label']
        if isinstance(ys[0], str):
            enums = list(set(ys))
            y_2_ix = {y: ix for ix, y in enumerate(enums)}
            y_indexs = [y_2_ix[y] for y in ys]
            test_data = [(x, int(y)) for x, y in zip(csv_df['text'], y_indexs)]
        else:
            test_data = [(x, int(y)) for x, y in zip(csv_df['text'], ys)]
        return test_data[:200]
    elif service == 'qa':
        raise EnvironmentError('currently does not support qa')
    elif service == 'qp':
        raise EnvironmentError('currently does not support qp')
    elif service == 'seq':
        test_data = read_seq_label_bio_data(data_path, 0, 1, output_alibaba=False, language='zh')[:200]
        return test_data


def online_test_general(service, data_path=''):
    config = {'ip': ips[service], 'port': ports[service], 'route': routes[service]}
    if data_path == '':
        test_data = get_service_test_data(service)
    else:
        test_data = service_data_reader(data_path, service)
    res = evaluate_online_model(test_data, service, config)
    print(res)


def online_test_seq():
    online_test_general('seq')


def online_test_cls():
    online_test_general('en_cls')


def online_test_qp():
    online_test_general('qp')


def online_test_qa():
    online_test_general('qa')


if __name__ == "__main__":
    # online_test_cls()
    # online_test_qp()
    # online_test_qa()
    # online_test_seq()
    main()
    # for i in range(1000):
    #     print(i)
    #     service_request('zh_cls')
    # clear_all_ngrok()
    # clear_all_screen()


def start_ngrok(service_name):
    cd_ngrok()
    run_cmd_get_rsp('pwd')
    cmd = get_ngrok_cmd(service_name)
    # cmd = "./ngrok -config=ngrok.cfg -subdomain={0} {1}:{2}".format(subdomain, yuhang_ip, port)
    os.system(cmd)
