import os
class Span():
    def __init__(self,span,label):
        self.span = span
        self.offset = [0,0]
        self.offset_from = 0
        self.label = label

    def set_offset(self,lr):
        self.offset = lr

    def set_from(self,f):
        self.offset_from = f

    def auto_set_to(self):
        self.offset = [self.offset_from,self.offset_from + len(self.span)]

    def to_dict(self):
        return {
            "span":self.span,
            "offset":self.offset
        }


def bert_symbol_cleaner(ws,ts):
    i = 0
    ws2 = []
    ts2 = []
    for w,t in zip(ws,ts):
        if w.startswith('##'):
            right_part = w[2:]
            ws2[-1] += right_part
        else:
            ws2.append(w)
            ts2.append(t)
    return ws2,ts2

def has_guess_token_in(tencent_row,true_token):
    for token in tencent_row:
        if token["BeginOffset"] == true_token["BeginOffset"]:
            return True
    return False

def is_inside(a,true_token):
    #  {'BeginOffset': 9, 'Type': 'GPE.NAM', 'Word': '沈阳店', 'Length': 3}]
    if a['BeginOffset']>= true_token['BeginOffset'] and \
            a['BeginOffset'] + a['Length']<= true_token['BeginOffset'] +true_token['Length']:
        return True
    return False

def has_intersection(a,true_token):
    if true_token['BeginOffset']<a['BeginOffset'] + a['Length']<= true_token['BeginOffset'] +true_token['Length']:
        # print(true_token['BeginOffset'])
        # print(a['BeginOffset'] + a['Length'])
        # print(true_token['BeginOffset'] +true_token['Length'])
        return True
    return False

def has_guess_the_position(predicted_tokens,true_token):
    for token in predicted_tokens:
        if has_intersection(token,true_token):
            return True
    return False

def find_predict_token_around(predicted_tokens,true_token):
    for token in predicted_tokens:
        if has_intersection(token, true_token):
            return token
    return None

def get_length_mistake_type(pred,true_token):
    #  {'BeginOffset': 9, 'Type': 'GPE.NAM', 'Word': '沈阳店', 'Length': 3}]
    if pred['BeginOffset'] != true_token['BeginOffset'] or \
            pred['BeginOffset'] + pred['Length']!=true_token['BeginOffset'] +true_token['Length']:
        return '长度不匹配'
    return ''

def get_ner_type_mistake_type(pred,true_token):
    pred_t = pred['Type']
    true_t = true_token['Type']
    if pred_t == true_t:
        return ''
    if pred_t.split('.')[0]!= true_t.split('.')[0]:
        return '主分类错误'
    if pred_t.split('.')[1]!= true_t.split('.')[1]:
        return '子分类错误'

def get_mistake(predicted_tokens,true_token):
    #print('实际', true_token)
    if not has_guess_the_position(predicted_tokens,true_token):
        return '未识别该实体'
    predicted = find_predict_token_around(predicted_tokens,true_token)
    if predicted is not None:
        #print('预测',predicted)
        length_mistake = get_length_mistake_type(predicted,true_token)
        ner_mistake = get_ner_type_mistake_type(predicted,true_token)
        if len(length_mistake)>0 and len(ner_mistake)>0:
            return length_mistake +'|'+ ner_mistake
        elif len(length_mistake)>0:
            return length_mistake
        elif len(ner_mistake)>0:
            return ner_mistake
        else:
            return '___ne'
    else:
        return '???no mistake'



def same_token(t1,t2):
    for k in t1:
        if k == 'Word':
            continue
        if t1[k] != t2[k]:
            return False
    return True

def convert_alibaba_to_bio(source_dict):
    l = len(source_dict.keys())
    for id in source_dict.keys():
        seq = source_dict[id]
        text = seq['content']
        records = seq['records']
        for tag_type in records.keys():
            span_list = records[tag_type]

def find_same_token_in(tencent_row,true_token):
    for token in tencent_row:
        if same_token(token , true_token):
            return True
    return False

def get_ner_dict(r):
    if isinstance(r,dict):
        return r
    return {
        'BeginOffset':r.BeginOffset,
        'Word':r.Word,
        'Length':r.Length,
        'Type':r.Type,
    }


def get_postag_dict(r):
    if isinstance(r,dict):
        return r
    return {
        'BeginOffset':r.BeginOffset,
        'Word':r.Word,
        'Length':r.Length,
        'Pos':r.Pos,
    }

def is_valid_bio(ws,ts):
    i = 0
    for w,t in zip(ws,ts):
        if t.startswith('I'):
            if i-1<0 or (ts[i-1] not in ['B-'+t[2:],'I-'+t[2:]]) :
                print(ws)
                print(ts)
                print(w,t)
                print(ts[i-1])
                return False
        i +=1
    return  True


def convert_pair_to_span_dict(words_for_line,tags_for_line,language = 'zh'):
    '''

    :param words_for_line: [w1,w2]
    :param tags_for_line: [t1,t2]
    :param language: zh or en
    :return:
    '''
    words_for_line,tags_for_line = bert_symbol_cleaner(words_for_line, tags_for_line)
    last_t = '_'
    spans = []
    span = None
    cur_char_i_for_span_start = 0

    if len(words_for_line) == 0:
        return {'content': ''.join(words_for_line), 'records': []}
    j = 0
    for w, t in zip(words_for_line, tags_for_line):
        j += 1
        raw_t = t if '-' not in t else t[2:]
        if t == 'O':
            if span is not None:
                spans.append(span)
            span = Span(w, t)
            s = cur_char_i_for_span_start
            if language == 'en':
                s = s + j - 1
                pre_seg = ' '.join([s.span for s in spans])
                s = len(pre_seg) + 1
            span.set_from(s)
            span.auto_set_to()
            cur_char_i_for_span_start += len(w)

        elif t.startswith('B-'):
            if span is not None:
                spans.append(span)
            span = Span(w, raw_t)
            s = cur_char_i_for_span_start
            if language == 'en':
                pre_seg = ' '.join([s.span for s in spans])
                s = len(pre_seg) + 1
            span.set_from(s)
            span.auto_set_to()
            cur_char_i_for_span_start += len(w)

        elif t.startswith('I-'):
            if span is None:
                ##print(words_for_line)
                #print(tags_for_line)
                #print(words_for_line[j - 1])
                pass
            if language == 'en':
                span.span += ' '
            # corruption data
            if span is None:
                #print(words_for_line)
                return {'records':[]}
            span.span += w
            span.auto_set_to()
            cur_char_i_for_span_start += len(w)

        if j == len(words_for_line):
            if span is not None:
                spans.append(span)
        if language and language == 'en':
            cur_char_i_for_span_start += 1
    records = {'records':[]}
    if language and language == 'en':

        content = ' '.join([s.span for s in spans])
    else:
        content = ''.join([s.span for s in spans])
    # print(len(content),spans[-1].offset)
    for span in spans:
        t = span.label
        if t != 'O':
            if t not in records:
                records[t] = [span.to_dict()]
            else:
                records[t].append(span.to_dict())
    ali_point = {'content': content, 'records': records}
    return ali_point


def read_seq_label_bio_data(filename, x_i, y_i,output_alibaba = True,language = 'en',long2short=False):
    print('abilibab filename',output_alibaba)
    if filename is None or filename == '':
        return [],[]
    tags = []
    id = 0
    invalid = 0
    data_tuples = []
    alibaba_data_json = {}
    # yuwei utf-8
    if os.path.exists(filename) == False:
        return None
    with open(filename,encoding='utf-8') as f:
        text = f.read()
        segments = text.split('\n\n')

        train = []
        for seg in segments:
            # seg is one point,line ,row
            lines = seg.splitlines()
            if lines == [] or len(lines)==0:
                continue
            words_for_line = []
            tags_for_line = []
            is_continue =False

            for line in lines:
                # print(line)
                if is_continue:
                    continue
                pair = line.split()

                if pair == [] or len(pair)<2:
                    continue
                # print(pair)
                word = pair[x_i]
                tag = pair[y_i]
                tags.append(tag)
                words_for_line.append(word)
                tags_for_line.append(tag)
                if not is_valid_bio(words_for_line, tags_for_line):
                    invalid += 1
                    print('invalid ', invalid)
                    is_continue = True
                    continue
            data_tuples.append((words_for_line,tags_for_line))
    return data_tuples

def convert_ali_to_tc(d,entity_map = None,ner=True):
    '''
    {'content': '1972年参加工作，曾任海军航空兵12师军械师，南京海军学院航空兵教研室正连职教官、淄博市化工公司团委副书记、山东张店化工厂工会主席、常务副厂长，山东东大化学工业（集团）公司董事长、总经理兼党委副书记，淄博市电子工业公司党委书记兼经理，现任本公司董事长、党委书记。', 'records': {'ORG': [{'span': '海军航空兵12师', 'offset': [12, 20]}, {'span': '南京海军学院航空兵教研室', 'offset': [24, 36]}, {'span': '淄博市化工公司', 'offset': [42, 49]}, {'span': '山东张店化工厂', 'offset': [55, 62]}, {'span': '山东东大化学工业（集团）公司', 'offset': [73, 87]}, {'span': '淄博市电子工业公司', 'offset': [101, 110]}, {'span': '本公司', 'offset': [120, 123]}], 'TITLE': [{'span': '军械师', 'offset': [20, 23]}, {'span': '正连职教官', 'offset': [36, 41]}, {'span': '团委副书记', 'offset': [49, 54]}, {'span': '工会主席', 'offset': [62, 66]}, {'span': '常务副厂长', 'offset': [67, 72]}, {'span': '董事长', 'offset': [87, 90]}, {'span': '总经理', 'offset': [91, 94]}, {'span': '党委副书记', 'offset': [95, 100]}, {'span': '党委书记', 'offset': [110, 114]}, {'span': '经理', 'offset': [115, 117]}, {'span': '董事长', 'offset': [123, 126]}, {'span': '党委书记', 'offset': [127, 131]}]}}
    [{'BeginOffset': 12, 'Type': 'ORG', 'Word': '海军航空兵12师', 'Length': 8}, {'BeginOffset': 24, 'Type': 'ORG', 'Word': '南京海军学院航空兵教研室', 'Length': 12}, {'BeginOffset': 42, 'Type': 'ORG', 'Word': '淄博市化工公司', 'Length': 7}, {'BeginOffset': 55, 'Type': 'ORG', 'Word': '山东张店化工厂', 'Length': 7}, {'BeginOffset': 73, 'Type': 'ORG', 'Word': '山东东大化学工业（集团）公司', 'Length': 14}, {'BeginOffset': 101, 'Type': 'ORG', 'Word': '淄博市电子工业公司', 'Length': 9}, {'BeginOffset': 120, 'Type': 'ORG', 'Word': '本公司', 'Length': 3}, {'BeginOffset': 20, 'Type': 'TITLE', 'Word': '军械师', 'Length': 3}, {'BeginOffset': 36, 'Type': 'TITLE', 'Word': '正连职教官', 'Length': 5}, {'BeginOffset': 49, 'Type': 'TITLE', 'Word': '团委副书记', 'Length': 5}, {'BeginOffset': 62, 'Type': 'TITLE', 'Word': '工会主席', 'Length': 4}, {'BeginOffset': 67, 'Type': 'TITLE', 'Word': '常务副厂长', 'Length': 5}, {'BeginOffset': 87, 'Type': 'TITLE', 'Word': '董事长', 'Length': 3}, {'BeginOffset': 91, 'Type': 'TITLE', 'Word': '总经理', 'Length': 3}, {'BeginOffset': 95, 'Type': 'TITLE', 'Word': '党委副书记', 'Length': 5}, {'BeginOffset': 110, 'Type': 'TITLE', 'Word': '党委书记', 'Length': 4}, {'BeginOffset': 115, 'Type': 'TITLE', 'Word': '经理', 'Length': 2}, {'BeginOffset': 123, 'Type': 'TITLE', 'Word': '董事长', 'Length': 3}, {'BeginOffset': 127, 'Type': 'TITLE', 'Word': '党委书记', 'Length': 4}]

    :param d: {'content': '1972年参加工作，曾任海军航空兵12师军械师，南京海军学院航空兵教研室正连职教官、淄博市化工公司团委副书记、山东张店化工厂工会主席、常务副厂长，山东东大化学工业（集团）公司董事长、总经理兼党委副书记，淄博市电子工业公司党委书记兼经理，现任本公司董事长、党委书记。', 'records': {'ORG': [{'span': '海军航空兵12师', 'offset': [12, 20]}, {'span': '南京海军学院航空兵教研室', 'offset': [24, 36]}, {'span': '淄博市化工公司', 'offset': [42, 49]}, {'span': '山东张店化工厂', 'offset': [55, 62]}, {'span': '山东东大化学工业（集团）公司', 'offset': [73, 87]}, {'span': '淄博市电子工业公司', 'offset': [101, 110]}, {'span': '本公司', 'offset': [120, 123]}], 'TITLE': [{'span': '军械师', 'offset': [20, 23]}, {'span': '正连职教官', 'offset': [36, 41]}, {'span': '团委副书记', 'offset': [49, 54]}, {'span': '工会主席', 'offset': [62, 66]}, {'span': '常务副厂长', 'offset': [67, 72]}, {'span': '董事长', 'offset': [87, 90]}, {'span': '总经理', 'offset': [91, 94]}, {'span': '党委副书记', 'offset': [95, 100]}, {'span': '党委书记', 'offset': [110, 114]}, {'span': '经理', 'offset': [115, 117]}, {'span': '董事长', 'offset': [123, 126]}, {'span': '党委书记', 'offset': [127, 131]}]}}
    the value of 'records'
    :param entity_map: None,to convert the tag
    :param ner: True for ner ,False for postagging
    :return:
    '''
    ## convert a alibaba dataset format to tc format
    tc_list = []
    # ds is {}record,key is entity
    for ent in d:
        # ent -> entity type or pos type
        if entity_map is None == True:
            if  not  entity_map is None :
                if ner :
                    if ent not in entity_map:
                        continue
                if not  ner:
                    if ent not in entity_map:
                        continue
        #print(ent)

        for span in d[ent]:
            #print('span')
            if ner and not  entity_map is None :
                ent = entity_map[ent]
            if ner:
                if isinstance(span,Span):
                    tc_d = {
                        "BeginOffset": span.offset[0],
                        "Type": ent,
                        "Word": span.span,
                        "Length": span.span
                    }
                else:
                    tc_d ={
                        "BeginOffset": span['offset'][0],
                        "Type": ent,
                        "Word": span['span'],
                        "Length": len(span['span'])
                      }
            else:
                ent2 = entity_map[ent]
                tc_d ={
                    "BeginOffset": span['offset'][0],
                    "Pos": ent2,
                    "Word": span['span'],
                    "Length": len(span['span'])
                  }
            tc_list.append(tc_d)
    return tc_list
def convert_ali_postag_to_tc(d,postag_map):
    ## convert a alibaba dataset format to tc format

    tc_list = []
    # ds is {}record,key is entity
    for ent in d:
        # ent -> entity type or pos type

        if ent is None or  ent not in postag_map:
            continue

        for span in d[ent]:
            print(ent)
            ent2 = postag_map[ent]
            tc_d ={
                "BeginOffset": span['offset'][0],
                "Pos": ent2,
                "Word": span['span'],
                "Length": len(span['span'])
              }

            tc_list.append(tc_d)
    return tc_list

def merge_tag(spans):
    for span in spans[:]:
        for span_right in spans[:]:
            if span["BeginOffset"]+ span['Length']== span_right['BeginOffset']:
                if 'Type' in span:
                    if span['Type'] == span_right['Type']:
                        spans.remove(span)
                        spans.remove(span_right)
                elif 'Pos' in span:
                    if span['Pos'] == span_right['Pos']:
                        spans.remove(span)
                        spans.remove(span_right)
    return spans


def compute_f1_by_token(tencent,correct_token,ner = True,valid_text= None):
    hit = 0
    guess = 0
    true = 0
    hit_cate  = {}
    guess_cate  = {}
    true_cate  = {}
    not_recall = 0
    mistake_type_d = {}
    wrong_predict = 0
    if ner:
        pos_or_type = 'Type'
    else:
        pos_or_type = 'Pos'
    line_i = 0
    for tencent_row,correct_token_row in zip(tencent,correct_token):
        #tencent_row = merge_tag(tencent_row)
        if tencent_row is None:
            line_i += 1
            continue
        if ner:

            tencent_row = [get_ner_dict(r) for r in tencent_row]
        else:

            tencent_row = [get_postag_dict(r) for r in tencent_row]

        for true_token in correct_token_row:
            if ner :
                # if true_token['Type'] not in valid_ner_tags:
                #     continue
                pass
            else:
                pass
                #if api_source == 'tencent':
                    # if true_token['Pos'] not in ['nr', 'n','nz']:
                    #      continue
                    #pass
                # else:
                #     if true_token['Pos'] not in ['noun']:
                #         continue

            tag = true_token[pos_or_type]
            if tag not in true_cate:
                true_cate[tag] = 1
            else:
                true_cate[tag] += 1
            true += 1
            is_guessed = has_guess_token_in(tencent_row,true_token)
            if is_guessed:
                if tag not in guess_cate:
                    guess_cate[tag] = 1
                else:
                    guess_cate[tag] += 1
                guess += 1
            if find_same_token_in(tencent_row,true_token):
                if tag not in hit_cate:
                    hit_cate[tag] = 1
                else:
                    hit_cate[tag] += 1
                hit += 1
            else:
                pass
                not_recall += 1
                # print('*'*100)
                # print(not_recall)
                # print(tencent_row)
                # print(true_token)
                #print('*' * 100)
                mistake_type = get_mistake(tencent_row, true_token)
                if not valid_text is None:

                    print(valid_text[line_i])
                    print(mistake_type)
                    if mistake_type is None:
                        continue
                    if mistake_type not in mistake_type_d:
                        mistake_type_d[mistake_type] = 1
                    else:
                        mistake_type_d[mistake_type] += 1

                # if is_guessed:
                #     #print('is_guessed')
                #     wrong_predict += 1
                #     print('-' * 100)
                #     print(wrong_predict)
                #     print(tencent_row)
                #     print(true_token)
                #     if not valid_text is None:
                #         print(valid_text[line_i])
        line_i += 1
    print(mistake_type_d)
    p = float(hit)/guess
    r = float(hit)/true
    f1 = 2* (p*r)/(p+r)
    l = 4
    res = f1
    print(round(f1,l),round(p,l),round(r,l))
    print('    f1      prec   recall')
    for cate in true_cate.keys():
        if cate not in guess_cate or cate not in true_cate or cate not in hit_cate:
            continue
        if cate not in hit_cate:
            hit_cate[cate] = 0
        p = float(hit_cate[cate]) / guess_cate[cate]
        r = float(hit_cate[cate]) / true_cate[cate]
        f1 = 2 * (p * r) / (p + r)
        print(cate,round(f1,4),round(p,4),round(r,4))
    return res

def word_tag_pair2tencent_format(ws,ts):
    a_dict = convert_pair_to_span_dict(ws, ts, language='zh')
    return convert_ali_to_tc(a_dict['records'])


