yuanheng_ip = '192.168.21.69'
haiqing_ip = '192.168.21.72'
yuhang_ip = '192.168.21.67'
local = yuhang_ip

ngrok_dirs = {
yuanheng_ip:"/home/socialbird/new_ngrok/ngrok",#"/home/socialbird/eric/ngrok",
yuhang_ip:'/home/socialbird/Documents/ngrok_new',
haiqing_ip:"/media/haiqing/data2/stella/ngrok_new"
}
# subdomains = {
#     'qa_inf':'nlp_qa_inf',
#     'qp_inf':'nlp_qp_inf',
#     'en_cls_inf':'nlp_cls_inf',
#     'zh_cls_inf':'nlp_zh_cls_inf',
#     'seqlab_inf':'nlp_seqlab_inf',
# }
# ips = {
#     'qa_inf':yuanheng_ip,
#     'qp_inf':yuanheng_ip,
#     'en_cls_inf':yuanheng_ip,
#     'zh_cls_inf':yuanheng_ip,
#     'seqlab_inf':haiqing_ip,
# }
# ports = {
#     'qa_inf':'38007',
#     'qp_inf':'38006',
#     'en_cls_inf':'38003',
#     'zh_cls_inf':'38008',
#     'seqlab_inf':'38002'
# }

servers = {
    'qa':'/home/socialbird/stella/aion-autonlp/autonlp/nlp_service',
    'qp':'/home/socialbird/stella/aion-autonlp/autonlp/nlp_service',
    'en_cls':'/home/socialbird/eric/aion-autonlp-master/autonlp/nlp_service',
    'zh_cls':'/home/socialbird/eric/aion-autonlp-master-ch/autonlp/nlp_service',
    'seq':'/media/haiqing/data2/stella/aion-autonlp/autonlp/nlp_service',
    'local':'/home/socialbird/platform/aion-autonlp/autonlp/nlp_service',
}
nlp_cmds = {
    'qa': 'python nlp_matching_inference_qa_service.py',
    'qp': 'python nlp_matching_inference_qp_service.py',
    'en_cls': 'python nlp_classification_service.py',
    'zh_cls': 'python nlp_classification_service.py',
    'seq': 'python nlp_ner_service.py --model_name no',
    'local': 'python nlp_ner_service.py --model_name no',
}
nlp_pys = {
    'qa': 'nlp_matching_inference_qa_service.py',
    'qp': 'nlp_matching_inference_qp_service.py',
    'en_cls': 'nlp_classification_service.py',
    'zh_cls': 'nlp_classification_service.py',
    'seq': 'nlp_ner_service.py',
    'local': 'nlp_ner_service.py',
}
subdomains = {
    'qa':'nlp_qa_inf',
    'qp':'nlp_qp_inf',
    'en_cls':'nlp_cls_inf',
    'zh_cls':'nlp_zh_cls_inf',
    'seq':'nlp_seqlab_inf',
    'local':'nlp_seqlab_inf',
}
ips = {
    'qa':yuanheng_ip,
    'qp':yuanheng_ip,
    'en_cls':yuanheng_ip,
    'zh_cls':yuanheng_ip,
    'seq':haiqing_ip,
    'local':yuhang_ip,
}
ports = {
    'qa':'38007',
    'qp':'38006',
    'en_cls':'38003',
    'zh_cls':'38008',
    'seq':'38002',
    'local':'38009'
}
routes = {
    'qa':'autonlp_QA_matching',
    'qp':'autonlp_QP_matching',
    'en_cls':'autonlp_classification',
    'zh_cls':'autonlp_classification',
    'seq':'autonlp_seqlabelling',
    'local':'autonlp_seqlabelling',
}
autonlp_demo_request_datas = {
    'qa': {
        "task_type": "matching",
        "task_data": {
            "q_text": "what is florence nightingale famous for ?",
            "d_text_list": [
                "the 2newly named `` corner shop '' where asprey has traded since     1847 occupies what were once nine georgian houses , one of which was the last london home of nursing pioneer florence nightingale .",
                "in 1820 , the founder of modern nursing , florence nightingale , was born in florence , italy .",
                "1836 _ theodor fliedner reinstitutes the order of deaconesses and opens a small hospital and training school in kaiserwerth , germany , where florence nightingale , `` the founder of modern nursing , '' receives her training .",
                "florence nightingale , a national heroine in the united kingdom , was the pioneer of the modern nursing .",
                "referring to the general acceptance of peplau 's interpersonal process , sills added , `` it has been argued that dr . peplau 's life and work produced the greatest changes in nursing practice since florence nightingale . ",
                "he noted that 21 people from 12 countries , including the three chinese nurses , won the 37th nightingale awards presented by the international committee of the red cross .",
                "the nightingale award , established at the ninth international red cross conference in washington in 1912 , are presented every two years ."
            ]
        }
    }
    ,
    'qp': {
        "task_type": "matching",
        "task_data": {
            "q_text": "还款日当日还款是否会产生逾期",
            "d_text_list": [
                "逾期一天要给多了利息"
            ]
        }
    },
    'en_cls': {
        "task_type": "cls",
        "language": "zh",
        "model_type": "bert",
        "task_data": [
            "hello this is 3242342342my teacheweqwe	wr",
            "Do you have iphone?I like HuaWei and Sumsumg"
        ]
    },
    'zh_cls': {
        "task_type": "cls",
        "language": "zh",
        "model_type": "bert",
        "task_data": [
            "我喜欢玩游戏",
            "我买了一件鞋子"

        ]
    },
    'seq': {
        "task_type": "rmrb_postag_2014",
        "language": "zh",
        "model_type": "bert",
        "task_data": [
            "马化腾，汉族潮汕人，1971年出生于广东省汕头市，1993年获深圳大学理学士学位。腾讯公司主要创办人之一。",
            "现任腾讯公司董事会主席兼首席执行官；全国青联副主席；全国人大代表。",
            "1988年，马云从杭州师范学院外国语系英语专业毕业，获文学学士学位，之后被分配到杭州电子工业学院(现杭州电子科技大学），任英文及国际贸易讲师。"

        ]
    }
}
